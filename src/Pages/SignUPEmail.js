import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  Button,
  TouchableHighlight,
  Image,
  Alert,
  Platform,
} from 'react-native';
import {Colors} from 'react-native/Libraries/NewAppScreen';

export default class SignUPEmail extends Component {
  constructor(props) {
    super(props);
    // eslint-disable-next-line no-undef
    state = {
      email: '',
      password: '',
    };
  }

  onClickListener = (viewId) => {
    Alert.alert('Alert', 'Button pressed ' + viewId);
  };

  render() {
    return (
      <View style={styles.mainContainer}>
        <View style={styles.ButtonViewStyles}>
          <Image
            style={styles.backArrowImage}
            source={require('../../assets/AppImages/backarrow.png')}
          />
        </View>
        <View style={styles.logoViewStyles}>
          <Image
            style={styles.LogoImage}
            source={require('../../assets/AppImages/logo.png')}
          />
        </View>
        <View style={styles.container}>
          <View style={styles.titleContainer}>
            <Text style={[styles.plainTextStyle, styles.titleBoldText]}>
              Create Account
            </Text>
            <Text style={[styles.plainTextStyle, styles.titleNormalText]}>
              Enter your email, phone and password.
            </Text>
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={[styles.inputs, styles.titleLightText]}
              placeholder="Email"
              keyboardType="default"
              underlineColorAndroid="transparent"
              // onChangeText={(email) => this.setState({email})}
            />
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={[styles.inputs, styles.titleLightText]}
              placeholder="Phone No."
              keyboardType="default"
              underlineColorAndroid="transparent"
              // onChangeText={(email) => this.setState({email})}
            />
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={[styles.inputs, styles.titleLightText]}
              placeholder="Password"
              keyboardType="default"
              underlineColorAndroid="transparent"
              // onChangeText={(email) => this.setState({email})}
            />
          </View>
          <View style={styles.inputContainer}>
            <TextInput
              style={[styles.inputs, styles.titleLightText]}
              placeholder="Confirm Password"
              keyboardType="default"
              underlineColorAndroid="transparent"
              // onChangeText={(email) => this.setState({email})}
            />
          </View>
        </View>
        <View style={styles.subContainer}>
          <TouchableHighlight
            style={[styles.buttonContainer, styles.loginButton]}
            onPress={() => this.onClickListener('Next')}>
            <Text style={[styles.loginText, styles.titleBoldText]}>Next</Text>
          </TouchableHighlight>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  titleContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 40,
  },
  mainContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    backgroundColor: '#DCDCDC', //'#F9FAFE',
    width: '100%',
    height: '100%',
    paddingTop: Platform.OS === 'ios' ? 44 : 0,
  },
  container: {
    // flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#DCDCDC', //'#F9FAFE',
    marginVertical: 100,
  },
  subContainer: {
    justifyContent: 'center',
    alignItems: 'flex-end',
    position: 'absolute',
    height: 50,
    bottom: 20,
  },
  plainTextStyle: {
    color: 'black',
    alignItems: 'flex-start',
    justifyContent: 'center',
  },
  inputContainer: {
    backgroundColor: '#FFFFFF',
    borderRadius: 8,
    shadowRadius: 5,
    shadowColor: 'black',
    shadowOpacity: 0.05,
    marginLeft: 25,
    marginRight: 25,
    height: 50,
    marginBottom: 25,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  inputs: {
    height: 45,
    flex: 1,
    textAlign: 'center',
  },
  buttonContainer: {
    height: 50,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    width: 180,
    borderRadius: 10,
  },
  loginButton: {
    backgroundColor: '#3366E5',
  },
  loginText: {
    color: 'white',
  },
  ButtonViewStyles: {
    justifyContent: 'flex-start',
    alignContent: 'center',
    flexDirection: 'row',
    height: 60,
    width: '100%',
  },
  logoViewStyles: {
    alignContent: 'flex-start',
    justifyContent: 'center',
    flexDirection: 'row',
    height: 60,
    width: '100%',
    top: -60,
  },
  backArrowImage: {
    resizeMode: 'contain',
    alignContent: 'flex-start',
    justifyContent: 'center',
    height: 60,
    marginLeft: 20,
  },
  LogoImage: {
    resizeMode: 'contain',
    height: 60,
  },
  titleBoldText: {
    // fontFamily: 'Calibri-Bold',
    fontSize: 20,
    fontWeight: 'bold',
    lineHeight: 40,
  },
  titleRegularText: {
    // fontFamily: 'Calibri-Regular',
    fontSize: 16,
    fontWeight: '200',
  },
  titleLightText: {
    // fontFamily: 'Calibri-Light',
    fontSize: 16,
    fontWeight: '200',
  },
});
