import * as React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import SignUPName from './src/Pages/SignUPName';
import SignUPEmail from './src/Pages/SignUPEmail';

const Stack = createStackNavigator();

function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="SignUPName">
        <Stack.Screen name="SignUPName" component={SignUPName} />
        <Stack.Screen name="SignUPEmail" component={SignUPEmail} />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
